import React, { useState, useEffect } from "react";
import ReactMapGL, { Marker, Popup } from "react-map-gl";
import * as destShop from "./components/data/shop-trajectoire.json";

export default function App() {
  const [viewport, setViewport] = useState({
    latitude: 48.856614,
    longitude: 2.3522219,
    width: "100vw",
    height: "100vh",
    zoom: 5
  });
  const [selectedShop, setSelectedShop] = useState(null);

  useEffect(() => {
    const listener = e => {
      if (e.key === "Escape") {
        setSelectedShop(null);
      }
    };
    window.addEventListener("keydown", listener);

    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, []);

  return (
    <div>
      <ReactMapGL
        {...viewport}
        mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
        mapStyle="mapbox://styles/nascvi/cjyr6ely80ah41dqtbqol43ct"
        onViewportChange={viewport => {
          setViewport(viewport);
        }}
      >
        {destShop.features.map(shop => (
          <Marker
            key={shop.properties.CLE}
            latitude={shop.geometry.coordinates[1]}
            longitude={shop.geometry.coordinates[0]}
          >
            <button
              className="marker-btn"
              onClick={e => {
                e.preventDefault();
                setSelectedShop(shop);
              }}
            >
              <img src="/shopLift.svg" alt="shopLift icon"/>
            </button>
          </Marker>
        ))}

        {selectedShop ? (
          <Popup
            latitude={selectedShop.geometry.coordinates[1]}
            longitude={selectedShop.geometry.coordinates[0]}
            onClose={() => {
              setSelectedShop(null);
            }}
          >
            <div>
              <h2 align="center">{selectedShop.properties.DEST_ID}</h2>
              <p align="center">
              <span><em>Longitude:</em> {selectedShop.geometry.coordinates[1]} </span>
              <br/>
              <span><em>Latitude:</em> {selectedShop.geometry.coordinates[0]}</span>
              </p>
            </div>
          </Popup>
        ) : null}
      </ReactMapGL>
    </div>
  );
          }
